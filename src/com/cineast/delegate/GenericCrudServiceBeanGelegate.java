package com.cineast.delegate;

import java.lang.reflect.Proxy;
import java.util.List;
import java.util.Map;

import com.cineast.locator.ServiceLocator;
import com.pidev.ejb.GenericCrudServiceBeanRemote;
import com.pidev.persistence.Message;
import com.pidev.persistence.Subject;
import com.pidev.persistence.Users;

public class GenericCrudServiceBeanGelegate {
	
	private static final String jndiName="ejb:/cineastejb/GenericCrudServiceBean!com.pidev.ejb.GenericCrudServiceBeanRemote";
	private static GenericCrudServiceBeanRemote getProxy(){
		return (GenericCrudServiceBeanRemote) ServiceLocator.getInstance().getProxy(jndiName);
	}

	
	public static <T> T create(T t) {
		// TODO Auto-generated method stub
		return getProxy().create(t);
	}

	
	public static <T> T find(Class<T> type, Object id) {
		// TODO Auto-generated method stub
		return getProxy().find(type, id);
	}

	
	public static <T> void delete(T t) {
		getProxy().delete(t);
		
	}

	
	public static <T> T update(T t) {
		// TODO Auto-generated method stub
		return getProxy().update(t);
	}

	
	public static List findWithNamedQuery(String queryName) {
		// TODO Auto-generated method stub
		return getProxy().findWithNamedQuery(queryName);
	}

	
	public static List findWithNamedQuery(String queryName, int resultLimit) {
		// TODO Auto-generated method stub
		return getProxy().findWithNamedQuery(queryName, resultLimit);
	}

	
	public static List findWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters) {
		// TODO Auto-generated method stub
		return getProxy().findWithNamedQuery(namedQueryName, parameters);
	}

	
	public static List findWithNamedQuery(String namedQueryName,
			Map<String, Object> parameters, int resultLimit) {
		// TODO Auto-generated method stub
		return getProxy().findWithNamedQuery(namedQueryName, parameters, resultLimit);
	}

	
	public static <T> List<T> findWithNativeQuery(String sql, Class<T> type) {
		// TODO Auto-generated method stub
		return getProxy().findWithNativeQuery(sql, type);
	}

	public static void assignSubjectToUser(Users user, Subject subject, String text) {
		getProxy().assignSubjectToUser(user, subject, text);
	}

}
