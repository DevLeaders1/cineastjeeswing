package com.cineast.locator;

public class ServiceLocatorException extends RuntimeException {
	
	public ServiceLocatorException(Throwable cause) {
		super(cause);
	}

}
