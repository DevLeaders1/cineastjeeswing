package com.cineast.ui;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.cineast.Model.MovieTableModel;
import com.cineast.delegate.GenericCrudServiceBeanGelegate;
import com.pidev.persistence.BoxOffice;
import com.pidev.persistence.Movies;

import javax.swing.JComboBox;
import java.awt.Color;


public class ManageMovie extends JFrame {

	public static MovieTableModel tableModel;  
	private ListSelectionModel lsm;
	
	private JPanel contentPane;
	private JFrame frame;
	private JTable Movietable;

	public JFrame getFrame() {
		return frame;
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ManageMovie frame = new ManageMovie();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ManageMovie() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 726, 472);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Manage Movie");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(319, 11, 361, 23);
		contentPane.add(lblNewLabel);

		JButton btnBannir = new JButton("AddToBoxOffice");
		btnBannir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 if (lsm == null) {
			            JOptionPane.showMessageDialog(null, "Selectionner une ligne", "Erreur de Selection", JOptionPane.ERROR_MESSAGE);
			        } else {
			            int minIndex = lsm.getMinSelectionIndex();
			            int maxIndex = lsm.getMaxSelectionIndex();
			            if ((maxIndex - minIndex) == 0) {
			                Object element = tableModel.getElementAt(minIndex);
			                Movies  m = (Movies) element;
			                BoxOffice b = GenericCrudServiceBeanGelegate.find(BoxOffice.class, 1);
			                m.setBoxoffice(b);
			                GenericCrudServiceBeanGelegate.update(m);
			                
			            } else {
			                JOptionPane.showMessageDialog(null, "Selectionner une seul ligne", "Erreur de Selection", JOptionPane.ERROR_MESSAGE);
			            }
			        }
				
				
				
				
				
				
				/*MovieSheet m=MovieSheetDelegate.Findbyid1(Integer.valueOf(id.getText()));
			BoxOffice boxOffice = null;
			boxOffice=BoxOfficeServiceDelegate.Findbyid1(1);
			m.setBoxOffice(boxOffice);
		MovieSheetDelegate.update(m);*/
				
			
			}
		});
		btnBannir.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnBannir.setBounds(147,354, 145, 23);
		contentPane.add(btnBannir);
        
        tableModel = new MovieTableModel();
         
         JScrollPane scrollPane = new JScrollPane();
         scrollPane.setBounds(24, 92, 656, 219);
         contentPane.add(scrollPane);
         
         Movietable = new JTable();
         scrollPane.setViewportView(Movietable);
         
         Movietable.setModel(tableModel);
         Movietable.setAutoCreateRowSorter(true);
         
         JButton btnConsultboxoffice = new JButton("ConsultBoxOffice");
         btnConsultboxoffice.addActionListener(new ActionListener() {
         	public void actionPerformed(ActionEvent e) {
         		new ManageBoxOffice().show();
         	}
         });
         btnConsultboxoffice.setFont(new Font("Tahoma", Font.BOLD, 12));
         btnConsultboxoffice.setBounds(424, 355, 154, 23);
         contentPane.add(btnConsultboxoffice);
         Movietable.getSelectionModel().addListSelectionListener(new ConsultationTableListener());
	}
	public class ConsultationTableListener implements ListSelectionListener {

        @Override
        public void valueChanged(ListSelectionEvent e) {
            lsm = (ListSelectionModel) e.getSource();
        }
    }
}
