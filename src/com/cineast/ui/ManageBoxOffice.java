package com.cineast.ui;

import java.awt.EventQueue;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import com.cineast.Model.BoxOfficeTableModel;
import com.cineast.Model.MovieTableModel;
import com.cineast.delegate.GenericCrudServiceBeanGelegate;
import com.cineast.ui.ManageMovie.ConsultationTableListener;
import com.pidev.persistence.BoxOffice;
import com.pidev.persistence.Movies;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;


public class ManageBoxOffice extends JFrame {
	public static BoxOfficeTableModel tableModel;  
	private ListSelectionModel lsm;
	
	
	private JPanel contentPane;
	private JFrame frame;
	private JTable table;
	private JTable BoxofficeTable;
	private DefaultTableModel model;

	public JFrame getFrame() {
		return frame;
	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ManageBoxOffice frame = new ManageBoxOffice();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ManageBoxOffice() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 726, 472);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Manage boxoffice");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblNewLabel.setBounds(319, 11, 361, 23);
		contentPane.add(lblNewLabel);
		/*
		 * JScrollPane scrollPane = new JScrollPane(); scrollPane.setBounds(239,
		 * 74, 421, 323); contentPane.add(scrollPane);
		 */

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setViewportView(table);
		scrollPane.setBounds(33, 81, 647, 198);
		contentPane.add(scrollPane);


		scrollPane.setViewportView(table);
		
		BoxofficeTable = new JTable();
		scrollPane.setViewportView(BoxofficeTable);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (lsm == null) {
		            JOptionPane.showMessageDialog(null, "Selectionner au moin une ligne", "Erreur de Selection", JOptionPane.ERROR_MESSAGE);
		        } else {
		            int p = JOptionPane.showConfirmDialog(null, "!voulez-vous vraiment supprimer  cet �l�ment?", "Supprimer", JOptionPane.YES_NO_OPTION);
		            if (p == 0) {
		                int minIndex = lsm.getMinSelectionIndex();
		                int maxIndex = lsm.getMaxSelectionIndex();
		                List elements = new ArrayList();
		                for (int i = minIndex; i <= maxIndex; i++) {
		                    if (lsm.isSelectedIndex(i)) {
		                        Object element = tableModel.getElementAt(i);
		                        elements.add(element);
		                    }
		                }
		                if (!tableModel.removeRows(elements)) {
		                    JOptionPane.showMessageDialog(null, "Erreur lors de la suppression ", "Erreur", JOptionPane.ERROR_MESSAGE);
		                } else {
		                    tableModel.fireTableDataChanged();
		                }
		            }
		        }
			}
		});
		btnDelete.setBounds(150, 346, 89, 23);
		contentPane.add(btnDelete);
		
		
//		String sql= "select * from Movies where boxoffice_id=1";
//		ArrayList<Movies> listeBox = (ArrayList<Movies>) 
//		GenericCrudServiceBeanGelegate.findWithNativeQuery(sql, Movies.class);
////System.out.println(listeUser);
//		
//		System.out.println(listeBox);
//
//		for (Movies boxs : listeBox) {
//
//	Object[] data = { boxs.getId(),boxs.getTitle()};
//			model.addRow(data);
//			
//	// rowData.add(users);
//		
//	}
		
		tableModel = new BoxOfficeTableModel();
		BoxofficeTable.setModel(tableModel);
		BoxofficeTable.setAutoCreateRowSorter(true);
		
		JButton button = new JButton();
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		button.setText("Close");
		button.setForeground(Color.WHITE);
		button.setBackground(new Color(204, 0, 0));
		button.setBounds(359, 346, 108, 23);
		contentPane.add(button);
		BoxofficeTable.getSelectionModel().addListSelectionListener(new ConsultationTableListener());
	}
	public class ConsultationTableListener implements ListSelectionListener {

       @Override
       public void valueChanged(ListSelectionEvent e) {
           lsm = (ListSelectionModel) e.getSource();
       }
	}
}
