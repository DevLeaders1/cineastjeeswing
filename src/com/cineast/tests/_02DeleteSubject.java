/**
 * 
 */
package com.cineast.tests;

import com.cineast.delegate.GenericCrudServiceBeanGelegate;
import com.pidev.persistence.Forum;
import com.pidev.persistence.Subject;

/**
 *
 * @author MarwenSdiri <marwen.sdiri@esprit.tn>
 *
 */
public class _02DeleteSubject {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Subject s= GenericCrudServiceBeanGelegate.find(Subject.class,6);
		GenericCrudServiceBeanGelegate.delete(s);
	}

}
