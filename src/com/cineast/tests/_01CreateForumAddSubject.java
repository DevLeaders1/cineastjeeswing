/**
 * 
 */
package com.cineast.tests;

import java.util.List;

import com.cineast.delegate.GenericCrudServiceBeanGelegate;
import com.pidev.persistence.Forum;
import com.pidev.persistence.Membres;
import com.pidev.persistence.Message;
import com.pidev.persistence.Subject;
import com.pidev.persistence.Users;

/**
 *
 * @author MarwenSdiri <marwen.sdiri@esprit.tn>
 *
 */
public class _01CreateForumAddSubject {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		
		
		//creation d'un forum 
				Forum f=new Forum("CineastForum");		
				GenericCrudServiceBeanGelegate.create(f);
				
		//creation des Sujets
				Subject s1 = new Subject("Soumettre un court m�trage", "Si vous souhaitez publier un de vos courts m�trages sur le forum, vous devez cr�er un sujet dans le forum D�p�t Court M�trages. N'oubliez pas de lire nos conseils de mise en forme de votre court m�trage.");
				Subject s2 = new Subject("Festival de Cannes 2014", "Le festival de Cannes se tiendra du 14 au 25 mai 2014, A noter que la Palme d'Or sera remise exceptionnellement le samedi soir � cause des �lections europ�ennes le dimanche.");
				Subject s3 = new Subject("Festival Armoricourt - 2015 - Appel � Films", "Les inscriptions sont ouvertes et la date de cl�ture est le 31/12/2014. Le festival Armoricourt, c'est en Bretagne - A Plestin Les Gr�ves (22) en Mars 2015");
				Subject s4 = new Subject("Silhouette 2014, 9�me �dition", "Silhouette pour sa 9�me �dition accueille le public au Parc des Buttes Chaumont.");
				Forum f2= GenericCrudServiceBeanGelegate.find(Forum.class,1);
				s1.setForum(f2);
				s2.setForum(f2);
				s3.setForum(f2);
				s4.setForum(f2);

				
				GenericCrudServiceBeanGelegate.create(s1);
				GenericCrudServiceBeanGelegate.create(s2);
				GenericCrudServiceBeanGelegate.create(s3);
				GenericCrudServiceBeanGelegate.create(s4);
				
			
				
				//create User
		Users user1 = new Users();
		user1.setFirstName("marwen");
		user1.setAddress("02,rue Beyrem Ettounsi");
		user1.setEmail("marwen.sdiri@rsprit.tn");
		user1.setLastName("sdiri");
		user1.setPassword("a");
	
		GenericCrudServiceBeanGelegate.create(user1);
		
		
		
		
		
		
		String sql="select * from Users where firstname='marwen'";
		Users u= GenericCrudServiceBeanGelegate.find(Users.class, 1);
		
		Subject suj = GenericCrudServiceBeanGelegate.find(Subject.class, 1);
		System.out.println(s1);
		
		// create message
			
		//      Message msg = new Message();
			//	msg.setText("hello s1 from user1 ");
			//	msg.setSubject(suj);
			//	msg.setUser(u.get(0));
			//	System.out.println(msg);
			//	GenericCrudServiceBeanGelegate.create(msg);
				
			//	Message m1 = GenericCrudServiceBeanGelegate.find(Message.class, 1);		
		GenericCrudServiceBeanGelegate.assignSubjectToUser(u, suj, "test test");
		
		
		

	}

}
