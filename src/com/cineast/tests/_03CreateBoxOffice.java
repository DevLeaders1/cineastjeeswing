/**
 * 
 */
package com.cineast.tests;

import java.util.Date;

import com.cineast.delegate.GenericCrudServiceBeanGelegate;
import com.pidev.persistence.BoxOffice;
import com.pidev.persistence.Forum;
import com.pidev.persistence.Movies;
import com.pidev.persistence.Users;

/**
 *
 * @author MarwenSdiri <marwen.sdiri@esprit.tn>
 *
 */
public class _03CreateBoxOffice {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		BoxOffice box = new BoxOffice();
		box.setType("action");
		//GenericCrudServiceBeanGelegate.create(box);
		
		Users owner= GenericCrudServiceBeanGelegate.find(Users.class,1);
		BoxOffice boxoffice= GenericCrudServiceBeanGelegate.find(BoxOffice.class,1);
		
		Movies m = new Movies("film4", new Date(), "romantic",14.25, "synopsis", "theme", owner, null);
		Movies m1 = new Movies("Film5", new Date(), "romantic",14.25, "synopsis", "theme", owner, null);
		Movies m2 = new Movies("Film6", new Date(), "romantic",14.25, "synopsis", "theme", owner, null);
		GenericCrudServiceBeanGelegate.create(m);
		GenericCrudServiceBeanGelegate.create(m1);
		GenericCrudServiceBeanGelegate.create(m2);
		
	}

}
