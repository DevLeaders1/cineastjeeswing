/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cineast.Model;


import com.cineast.delegate.GenericCrudServiceBeanGelegate;
import com.pidev.persistence.Movies;
import com.pidev.persistence.Movies;
import com.pidev.persistence.Movies;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asd
 */
public class BoxOfficeTableModel extends ConsultationTableModel {

	private String title[] = {"id","Title", "dateOfRealise", "genre", "trailer","budget","synopsis","theme"};
	private List<Movies> listMovies= new ArrayList<Movies>();
    private List<Movies> listResultSearch = new ArrayList<Movies>();
    private boolean searching = false;
    private int me;

    
    /**
	 * 
	 */
	public BoxOfficeTableModel() {
		selectMovies();
	}

	private void selectMovies() {
      
		String sql= "select * from Movies where boxoffice_id=1";
            this.listMovies = GenericCrudServiceBeanGelegate.findWithNativeQuery(sql, Movies.class);       
    }
    
    @Override
    public Object getElementAt(int i) {
    	return listMovies.get(i);
    }

    @Override
    public boolean removeRows(List elements) {
    	boolean done = true;
       
    	List<Movies> lSub = (List<Movies>) elements;
        for (int i = 0; i < lSub.size(); i++) {
           
            	GenericCrudServiceBeanGelegate.delete(lSub.get(i));
                listMovies.remove(lSub.get(i));
            
        }
        return done;
    }

    @Override
    public void refresh() {
        listResultSearch = new ArrayList<Movies>();
        selectMovies();
    }

    @Override
    public void initSearch(String text, int selectedIndex) {
    	 
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
    	
    	switch (columnIndex) {
		case 0:
			
			return listMovies.get(rowIndex).getId();
		case 1:
			return listMovies.get(rowIndex).getTitle();
		case 2:
			return listMovies.get(rowIndex).getDateOfRealise();
		case 3:
			return listMovies.get(rowIndex).getGenre();
		case 4:
			return listMovies.get(rowIndex).getTrailer();
		case 5:
			return listMovies.get(rowIndex).getBudget();
		case 6:
			return listMovies.get(rowIndex).getSynopsis();
		case 7:
			return listMovies.get(rowIndex).getTheme();
		default:
			return null;
		}
    }

    @Override
    public int getColumnCount() {
        return title.length;
    }

    @Override
    public int getRowCount() {
        if (searching && listResultSearch.size() > 0) {
            return listResultSearch.size();
        } else if (searching && listResultSearch.size() == 0) {
            return 0;
        } else {
            return listMovies.size();
        }
    }
    public String getColumnName(int col) {
        return this.title[col];
    }

}
