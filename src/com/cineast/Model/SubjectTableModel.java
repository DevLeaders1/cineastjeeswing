/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cineast.Model;


import com.cineast.delegate.GenericCrudServiceBeanGelegate;
import com.pidev.persistence.Subject;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author asd
 */
public class SubjectTableModel extends ConsultationTableModel {

    private String title[] = {"idSubject","Title", "Date", "Time", "Description"};
    private List<Subject> listSubject= new ArrayList<Subject>();
    private List<Subject> listResultSearch = new ArrayList<Subject>();
    private boolean searching = false;
    private int me;

    
    /**
	 * 
	 */
	public SubjectTableModel() {
		selectSubject();
	}

	private void selectSubject() {
      
        	String sql = "select * from Subject ";
            this.listSubject = GenericCrudServiceBeanGelegate.findWithNativeQuery(sql, Subject.class);       
    }
    
    @Override
    public Object getElementAt(int i) {
    	return listSubject.get(i);
    }

    @Override
    public boolean removeRows(List elements) {
    	boolean done = true;
       
    	List<Subject> lSub = (List<Subject>) elements;
        for (int i = 0; i < lSub.size(); i++) {
           
            	GenericCrudServiceBeanGelegate.delete(lSub.get(i));
                listSubject.remove(lSub.get(i));
            
        }
        return done;
    }

    @Override
    public void refresh() {
        listResultSearch = new ArrayList<Subject>();
        selectSubject();
    }

    @Override
    public void initSearch(String text, int selectedIndex) {
    	 listResultSearch = new ArrayList<Subject>();
         if (text.length() > 0) {
             searching = true;
             for (Subject subject : listSubject) {
                 if (selectedIndex == 0) {
                     if (subject.getTitle() != null) {
                         if (subject.getTitle().toUpperCase().matches("(.*)" + text.toUpperCase() + "(.*)")) {
                             listResultSearch.add(subject);
                         }
                     }
                 } else if (selectedIndex == 1) {
                     if (subject.getDate() != null) {
                         if (subject.getDate().toString().toUpperCase().matches("(.*)" + text.toUpperCase() + "(.*)")) {
                             listResultSearch.add(subject);
                         }
                     }
                 } else if (selectedIndex == 2) {
                     if (subject.getTime() != null) {
                         if (subject.getTime().toUpperCase().matches("(.*)" + text.toUpperCase() + "(.*)")) {
                             listResultSearch.add(subject);
                         }
                     }
                
                 } else if (selectedIndex == 3) {
                     if (subject.getDescription() != null) {
                         if (subject.getDescription().toString().toUpperCase().matches("(.*)" + text.toUpperCase() + "(.*)")) {
                             listResultSearch.add(subject);
                         }
                     }
                 } 
             }
         } else {
             searching = false;
         }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
    	switch (columnIndex) {
		case 0:
			return listSubject.get(rowIndex).getIdSubject();
		case 1:
			return listSubject.get(rowIndex).getTitle();
		case 2:
			return listSubject.get(rowIndex).getDate();
		case 3:
			return listSubject.get(rowIndex).getTime();
		case 4:
			return listSubject.get(rowIndex).getDescription();
		
		default:
			return null;
		}
    }

    @Override
    public int getColumnCount() {
        return title.length;
    }

    @Override
    public int getRowCount() {
        if (searching && listResultSearch.size() > 0) {
            return listResultSearch.size();
        } else if (searching && listResultSearch.size() == 0) {
            return 0;
        } else {
            return listSubject.size();
        }
    }
    public String getColumnName(int col) {
        return this.title[col];
    }

}
