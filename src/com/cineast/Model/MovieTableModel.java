/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cineast.Model;


import com.cineast.delegate.GenericCrudServiceBeanGelegate;
import com.pidev.persistence.Movies;
import com.pidev.persistence.Movies;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author CHAIMA
 */
public class MovieTableModel extends ConsultationTableModel {

    private String title[] = {"id","Title", "dateOfRealise", "genre", "trailer","budget","synopsis","theme"};
    private List<Movies> listMovies= new ArrayList<Movies>();
    private List<Movies> listResultSearch = new ArrayList<Movies>();
    private boolean searching = false;
    private int me;

    
    /**
	 * 
	 */
	public MovieTableModel() {
		selectMovies();
	}

	private void selectMovies() {
      
        	String sql = "select * from Movies ";
            this.listMovies = GenericCrudServiceBeanGelegate.findWithNativeQuery(sql, Movies.class);       
    }
    
    @Override
    public Object getElementAt(int i) {
    	return listMovies.get(i);
    }

    @Override
    public boolean removeRows(List elements) {
    	boolean done = true;
       
    	List<Movies> lSub = (List<Movies>) elements;
        for (int i = 0; i < lSub.size(); i++) {
           
            	GenericCrudServiceBeanGelegate.delete(lSub.get(i));
                listMovies.remove(lSub.get(i));
            
        }
        return done;
    }

    @Override
    public void refresh() {
        listResultSearch = new ArrayList<Movies>();
        selectMovies();
    }

    @Override
    public void initSearch(String text, int selectedIndex) {
    	 listResultSearch = new ArrayList<Movies>();
         if (text.length() > 0) {
             searching = true;
             for (Movies Movies : listMovies) {
                 if (selectedIndex == 0) {
                     if (Movies.getTitle() != null) {
                         if (Movies.getTitle().toUpperCase().matches("(.*)" + text.toUpperCase() + "(.*)")) {
                             listResultSearch.add(Movies);
                         }
                     }
                 } else if (selectedIndex == 1) {
                     if (Movies.getDateOfRealise() != null) {
                         if (Movies.getDateOfRealise().toString().toUpperCase().matches("(.*)" + text.toUpperCase() + "(.*)")) {
                             listResultSearch.add(Movies);
                         }
                     }
                 } else if (selectedIndex == 2) {
                     if (Movies.getGenre() != null) {
                         if (Movies.getGenre().toUpperCase().matches("(.*)" + text.toUpperCase() + "(.*)")) {
                             listResultSearch.add(Movies);
                         }
                     }
                
                 } else if (selectedIndex == 3) {
                     if (Movies.getTrailer() != null) {
                         if (Movies.getTrailer().toString().toUpperCase().matches("(.*)" + text.toUpperCase() + "(.*)")) {
                             listResultSearch.add(Movies);
                         }
                     }
                  
             } else if (selectedIndex == 4) {
                 if (Movies.getTheme() != null) {
                     if (Movies.getTheme().toUpperCase().matches("(.*)" + text.toUpperCase() + "(.*)")) {
                         listResultSearch.add(Movies);
                     }
                 }
             } else if (selectedIndex == 5) {
                 if (Movies.getSynopsis() != null) {
                     if (Movies.getSynopsis().toUpperCase().matches("(.*)" + text.toUpperCase() + "(.*)")) {
                         listResultSearch.add(Movies);
                     }
                 }
         } else {
             searching = false;
         }
    }
         }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
    	switch (columnIndex) {
		case 0:
			return listMovies.get(rowIndex).getId();
		case 1:
			return listMovies.get(rowIndex).getTitle();
		case 2:
			return listMovies.get(rowIndex).getDateOfRealise();
		case 3:
			return listMovies.get(rowIndex).getGenre();
		case 4:
			return listMovies.get(rowIndex).getTrailer();
		case 5:
			return listMovies.get(rowIndex).getBudget();
		case 6:
			return listMovies.get(rowIndex).getSynopsis();
		case 7:
			return listMovies.get(rowIndex).getTheme();
		default:
			return null;
		}
    }

    @Override
    public int getColumnCount() {
        return title.length;
    }

    @Override
    public int getRowCount() {
        if (searching && listResultSearch.size() > 0) {
            return listResultSearch.size();
        } else if (searching && listResultSearch.size() == 0) {
            return 0;
        } else {
            return listMovies.size();
        }
    }
    public String getColumnName(int col) {
        return this.title[col];
    }

}
